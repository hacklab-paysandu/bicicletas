import React, { useEffect, useState, useContext } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet, Alert } from 'react-native';

import { Context as AuthContext } from '../context/AuthContext';

import BotonFlotando from '../components/BotonFlotando';

const TelefonoScreen = ({ navigation }) => {
	const [telefonoInput, setTelefonoInput] = useState( null );
	const [telefono, setTelefono] = useState( '' );
	const { obtenerCodigo } = useContext( AuthContext );

	useEffect( () => {
		if ( telefonoInput )
			telefonoInput.focus();
	}, [telefonoInput] );

	const confirmar = () => {
		if ( telefono.length != 9 )
			return Alert.alert( 'Ingrese número de 9 dígitos.' );

		obtenerCodigo( telefono, () => {
			navigation.navigate( 'Codigo', { telefono } );
		}, ( err ) => {
			Alert.alert( err );
		} );
	}

	return (
		<View style={ styles.container }>
			<Text style={ styles.texto }>Ingrese número de celular:</Text>

			<View style={ styles.codigoContainer }>
				<TextInput
					ref={ ( input ) => setTelefonoInput( input ) }
					style={ styles.input }
					maxLength={ 9 }
					keyboardType={ 'numeric' }
					onChangeText={ ( telefono ) => {
						setTelefono( telefono );
					} }
					onSubmitEditing={ confirmar } />
			</View>

			<BotonFlotando
				title="CONTINUAR"
				onPress={ confirmar } />
		</View>
	);
};

TelefonoScreen.navigationOptions = {
	title: 'Entrar'
};

const styles = StyleSheet.create( {
	container: {
		flex: 1
	},
	texto: {
		fontSize: 22,
		textAlign: 'justify',
		marginTop: 20,
		marginBottom: 0,
		marginHorizontal: 40
	},
	codigoContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginHorizontal: 40,
		marginTop: 20
	},
	input: {
		flex: 1,
		height: 60,

		textAlign: 'center',
		fontSize: 30,

		backgroundColor: '#DDD',
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#CCC'
	}
} );

export default TelefonoScreen;

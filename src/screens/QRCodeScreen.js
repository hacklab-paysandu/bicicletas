import React, { useState, useEffect, useContext } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';

import { Context as QRCodeContext } from '../context/QRCodeContext';

import BotonFlotando from '../components/BotonFlotando';

const QRCodeScreen = ({ navigation }) => {
	const { setQRCode } = useContext( QRCodeContext );

	const [scanned, setScanned] = useState(false);

	useEffect( () => {
		( async () => {
			await BarCodeScanner.requestPermissionsAsync();
		} ) ();
	}, [] );

	const _handleBarCodeScanned = ({ type, data }) => {
		if ( data !== '' )
			setQRCode( data );
		navigation.navigate( 'Main' );
	};

	const cancelar = () => {
		navigation.navigate( 'Main' );
	};

	return (
		<View style={ styles.container }>
			<BarCodeScanner
				style={ styles.camara }
				onBarCodeScanned={ _handleBarCodeScanned } />

			<BotonFlotando
				title="CANCELAR"
				onPress={ cancelar } />
		</View>
	);
};

QRCodeScreen.navigationOptions = {
	headerShown: false
};

const styles = StyleSheet.create( {
	container: {
		flex: 1,
		backgroundColor: 'black'
	},
	camara: {
		flex: 8
	},
	botonContainer: {
		position: 'absolute',

		left: 0,
		right: 0,
		bottom: 0,

		padding: 50,

		backgroundColor: 'white'
	}
} );

export default QRCodeScreen;

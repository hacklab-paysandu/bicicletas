import React from 'react';
import { View, StyleSheet, Alert } from 'react-native';

import BotonFlotando from '../components/BotonFlotando';

const MainScreen = ({ navigation }) => {

	const entrar = () => {
		navigation.navigate( 'Telefono' );
	}

	return (
		<View style={ styles.container }>

			<BotonFlotando
				title="ENTRAR"
				onPress={ entrar } />
		</View>
	);
};

MainScreen.navigationOptions = {
	headerShown: false
};

const styles = StyleSheet.create( {
	container: {
		flex: 1
	}
} );

export default MainScreen;

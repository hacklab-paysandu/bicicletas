import React, { useEffect, useState, useContext } from 'react';
import { View, Text, TextInput, StyleSheet, Alert } from 'react-native';

import { Context as AuthContext } from '../context/AuthContext';

import BotonFlotando from '../components/BotonFlotando';

const CodigoScreen = ({ navigation }) => {
	const telefono = navigation.getParam( 'telefono' );

	const [primerInput, setPrimerInput] = useState( null );
	const [segundoInput, setSegundoInput] = useState( null );
	const [tercerInput, setTercerInput] = useState( null );
	const [cuartoInput, setCuartoInput] = useState( null );

	const [primerDigito, setPrimerDigito] = useState( '' );
	const [segundoDigito, setSegundoDigito] = useState( '' );
	const [tercerDigito, setTercerDigito] = useState( '' );
	const [cuartoDigito, setCuartoDigito] = useState( '' );

	const { validarCodigo } = useContext( AuthContext );

	const confirmar = () => {
		if ( primerDigito && segundoDigito && tercerDigito && cuartoDigito )
		{
			const codigo = primerDigito + segundoDigito + tercerDigito + cuartoDigito;
			
			validarCodigo( telefono, codigo, () => {
				navigation.navigate( 'mainFlow' );
			}, ( err ) => {
				Alert.alert( err );
			} );
		}
	}

	useEffect( () => {
		if ( primerInput )
			primerInput.focus();
	}, [primerInput] );

	return (
		<View style={ styles.container }>
			<Text style={ styles.texto }>Se le enviará un código de cuatro dígitos por mensaje de texto.</Text>
			<Text style={ styles.texto }>CÓDIGO:</Text>

			<View style={ styles.codigoContainer }>
				<TextInput
					ref={ ( input ) => setPrimerInput( input ) }
					style={ styles.input }
					maxLength={ 1 }
					keyboardType={ 'numeric' }
					onChangeText={ ( texto ) => {
						if ( texto.length == 1 )
							segundoInput.focus();

						setPrimerDigito( texto );
					} } />

				<TextInput
					ref={ ( input ) => setSegundoInput( input ) }
					style={ styles.input }
					maxLength={ 1 }
					keyboardType={ 'numeric' }
					onChangeText={ ( texto ) => {
						if ( texto.length == 1 )
							tercerInput.focus();

						setSegundoDigito( texto );
					} } />
				
				<TextInput
					ref={ ( input ) => setTercerInput( input ) }
					style={ styles.input }
					maxLength={ 1 }
					keyboardType={ 'numeric' }
					onChangeText={ ( texto ) => {
						if ( texto.length == 1 )
							cuartoInput.focus();

						setTercerDigito( texto );
					} } />
				
				<TextInput
					ref={ ( input ) => setCuartoInput( input ) }
					style={ styles.input }
					maxLength={ 1 }
					keyboardType={ 'numeric' }
					onChangeText={ ( texto ) => {
						setCuartoDigito( texto );
					} }
					onSubmitEditing={ confirmar } />
			</View>

			<BotonFlotando
				title="CONFIRMAR"
				onPress={ confirmar } />
		</View>
	);
};

CodigoScreen.navigationOptions = {
	title: 'Validar código'
};

const styles = StyleSheet.create( {
	container: {
		flex: 1
	},
	texto: {
		fontSize: 22,
		textAlign: 'justify',
		marginTop: 20,
		marginBottom: 0,
		marginHorizontal: 40
	},
	codigoContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginHorizontal: 40,
		marginTop: 20
	},
	input: {
		width: 40,
		height: 60,

		textAlign: 'center',
		fontSize: 30,

		backgroundColor: '#DDD',
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#CCC'
	}
} );

export default CodigoScreen;

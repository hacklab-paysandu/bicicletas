import React, { useEffect, useContext } from 'react';
import { View, Alert, TouchableOpacity, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { BarCodeScanner } from 'expo-barcode-scanner';

import { Context as QRCodeContext } from '../context/QRCodeContext';
import { Context as AuthContext } from '../context/AuthContext';
import { Context as BicicletasContext } from '../context/BicicletasContext';

import BotonFlotando from '../components/BotonFlotando';
import Mapa from '../components/Mapa';

const MainScreen = ({ navigation }) => {
	const { state: { qr_code }, setQRCode } = useContext( QRCodeContext );
	const { cerrarSesion } = useContext( AuthContext );
	const { state: { id_bicicleta }, obtenerIDBicicleta, ocuparBicicleta, devolverBicicleta } = useContext( BicicletasContext );

	useEffect( () => {
		navigation.setParams({ cerrarSesion });

		if ( id_bicicleta === null )
			obtenerIDBicicleta();
	}, [] );

	useEffect( () => {
		if ( id_bicicleta === '' && qr_code !== null )
			ocuparBicicleta(
				qr_code,
				() => Alert.alert( 'Bicicleta obtenida.' ),
				err => {
					setQRCode( null );
					Alert.alert( err );
				}
			);
		else if ( id_bicicleta !== null && id_bicicleta !== '' && qr_code !== null )
			devolverBicicleta(
				qr_code,
				() => Alert.alert( 'Bicicleta devuelta.' ),
				err => {
					setQRCode( null );
					Alert.alert( err );
				}
			);
	}, [qr_code] );

	const escanearCodigo = () => {
		BarCodeScanner.requestPermissionsAsync()
			.then( ( data ) => {
				if ( data.status === 'granted' )
					navigation.navigate( 'QRCode' );
				else
					Alert.alert( 'Debe dar permisos para utilizar la cámara.' );
			} )
			.catch( ( err ) => {
				Alert.alert( 'Debe dar permisos para utilizar la cámara.' );
			} )
	};

	return (
		<View style={ styles.container }>
			<Mapa />

			{ id_bicicleta === '' ?
				<BotonFlotando
					title="ESCANEAR CÓDIGO"
					onPress={ escanearCodigo } />
			: null }

			{ id_bicicleta !== null && id_bicicleta !== '' ?
				<BotonFlotando
					title="DEVOLVER BICICLETA"
					onPress={ escanearCodigo } />
			: null }
		</View>
	);
};

MainScreen.navigationOptions = ({ navigation }) => {
	return {
		headerTitle: 'Bicicleteros',
		headerRight: () => (
			<TouchableOpacity style={{ paddingRight: 20 }} onPress={ navigation.getParam( 'cerrarSesion' ) }>
				<Ionicons name="md-exit" size={ 30 } />
			</TouchableOpacity>
		)
	};
};

const styles = StyleSheet.create( {
	container: {
		flex: 1
	}
} );

export default MainScreen;

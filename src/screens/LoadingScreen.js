import React, { useEffect, useContext } from 'react';

import { Context as AuthContext } from '../context/AuthContext';

const LoadingScreen = () => {
	const { iniciarSesionLocalmente } = useContext( AuthContext );

	useEffect( () => {
		iniciarSesionLocalmente();
	}, [] );

	return null;
};

export default LoadingScreen;

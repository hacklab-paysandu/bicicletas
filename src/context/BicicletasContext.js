import createDataContext from './createDataContext';
import servidor from '../api/servidor';

const BicicletasReducer = ( state, action ) => {
	switch ( action.type ) {
		case 'set_bicicleteros' :
			return { ...state, bicicleteros: action.payload };
		case 'id_bicicleta' :
			return { ...state, id_bicicleta: action.payload };
		default :
			return state;
	}
};

const obtenerBicicleteros = ( dispatch ) => {
	return async ( success, error ) => {
		let response = await servidor.get( '/obtener_bicicleteros' );

		if ( response.data.state === 'OK' )
		{
			dispatch({ type: 'set_bicicleteros', payload: response.data.message });

			if ( success )
				success();
		}

		if ( response.data.state === 'ERR' && error )
			error( response.data.message );
	};
};

const obtenerIDBicicleta = ( dispatch ) => {
	return async () => {
		let response = await servidor.get( '/tiene_bicicleta_ocupada' );

		if ( response.data.state === 'OK' )
			dispatch({ type: 'id_bicicleta', payload: response.data.message ? response.data.message : '' });
	};
};

const ocuparBicicleta = ( dispatch ) => {
	return async ( id_bicicleta, success, error ) => {
		let response = await servidor.post( '/ocupar_bicicleta', { id_bicicleta } );

		if ( response.data.state === 'OK' )
		{
			dispatch({ type: 'id_bicicleta', payload: response.data.message.identificador });

			if ( success )
				success();
		}
		else if ( error )
			error( response.data.message );
	};
};

const devolverBicicleta = ( dispatch ) => {
	return async ( data, success, error ) => {
		const [ id_bicicletero, posicion ] = data.split( ';' );
		let response = await servidor.post( '/devolver_bicicleta', { id_bicicletero, posicion } );

		if ( response.data.state === 'OK' )
		{
			dispatch({ type: 'id_bicicleta', payload: '' });

			if ( success )
				success();
		}
		else if ( error )
			error( response.data.message );
	};
};

export const { Context, Provider } = createDataContext(
	BicicletasReducer,
	{ obtenerIDBicicleta, obtenerBicicleteros, ocuparBicicleta, devolverBicicleta },
	{ bicicleteros: [], id_bicicleta: null }
);
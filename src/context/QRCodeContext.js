import createDataContext from './createDataContext';

const QRCodeReducer = ( state, action ) => {
	switch ( action.type ) {
		case 'set_code' :
			return { ...state, qr_code: action.payload };
		default :
			return state;
	}
};

const setQRCode = ( dispatch ) => {
	return ( qr_code ) => {
		dispatch({ type: 'set_code', payload: qr_code });
	};
};

export const { Context, Provider } = createDataContext(
	QRCodeReducer,
	{ setQRCode },
	{ qr_code: null }
);
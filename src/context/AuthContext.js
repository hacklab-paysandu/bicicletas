import createDataContext from './createDataContext';
import { AsyncStorage } from 'react-native';
import servidor from '../api/servidor';
import { navigate } from '../navigationRef';

const AuthReducer = ( state, action ) => {
	switch ( action.type ) {
		default :
			return state;
	}
};

const iniciarSesionLocalmente = ( dispatch ) => {
	return async () => {
		const token = await AsyncStorage.getItem( 'token' );

		if ( token )
			navigate( 'mainFlow' );
		else
			navigate( 'iniciarFlow' );
	};
};

const obtenerCodigo = ( dispatch ) => {
	return async ( telefono, success, error ) => {
		const response = await servidor.post( '/obtener_codigo', { telefono } );

		if ( response.data.state === 'OK' && success )
			success();

		if ( response.data.state === 'ERR' && error )
			error( response.data.state.message );
	};
};

const validarCodigo = ( dispatch ) => {
	return async ( telefono, codigo, success, error ) => {
		const response = await servidor.post( '/validar_codigo', { telefono, codigo } );

		if ( response.data.state === 'OK' )
		{
			await AsyncStorage.setItem( 'token', response.data.message );
			
			if ( success )
				success();
		}

		if ( response.data.state === 'ERR' && error )
			error( response.data.message );
	};
};

const cerrarSesion = ( dispatch ) => {
	return async () => {
		await AsyncStorage.removeItem( 'token' );
		navigate( 'Loading' );
	};
};

export const { Context, Provider } = createDataContext(
	AuthReducer,
	{ iniciarSesionLocalmente, obtenerCodigo, validarCodigo, cerrarSesion },
	{}
);
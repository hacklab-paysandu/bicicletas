import React from 'react';
import { View, StyleSheet } from 'react-native';

import Boton from './Boton';

const BotonFlotando = ({ title, onPress }) => {
	return (
		<View style={ styles.container }>
			<Boton
				title={ title }
				onPress={ onPress } />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		position: 'absolute',

		left: 0,
		right: 0,
		bottom: 0,
		padding: 50
	}
});

export default BotonFlotando;

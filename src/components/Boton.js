import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const Boton = ({ title, onPress }) => {
	return (
		<TouchableOpacity style={ styles.boton } onPress={ onPress }>
			<Text style={ styles.texto }>{ title }</Text>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create( {
	boton: {
		height: 40,
		justifyContent: 'center',
		paddingVertical: 5,
		borderRadius: 20,
		backgroundColor: '#3c78d8ff'
	},
	texto: {
		fontSize: 18,
		color: 'white',
		textAlign: 'center'
	}
} );

export default Boton;

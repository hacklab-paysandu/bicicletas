import React, { useEffect, useContext } from 'react';
import { StyleSheet } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import config from '../../config';

import { Context as BicicletasContext } from '../context/BicicletasContext';

const Mapa = () => {
	const { state: { bicicleteros }, obtenerBicicleteros } = useContext( BicicletasContext );

	useEffect( () => {
		obtenerBicicleteros();
	}, [] );

	return (
		<>
			<MapView
				style={ styles.mapa }
				minZoomLevel={ 13 }
				maxZoomLevel={ 16 }
				initialRegion={{
					latitude: config.latitudInicial,
					longitude: config.longitudInicial,
					latitudeDelta: 0.03,
					longitudeDelta: 0.03
				}}>
				{ bicicleteros.map( bicicletero => (
					<Marker
						key={ bicicletero.nombre }
						coordinate={{
							latitude: bicicletero.latitud,
							longitude: bicicletero.longitud
						}}
						title={ bicicletero.nombre ? bicicletero.nombre : null }
						description={ bicicletero.ubicacion ? bicicletero.ubicacion : null } />
				) ) }

			</MapView>
		</>
	);
};

const styles = StyleSheet.create( {
	mapa: {
		flex: 1
	}
} );

export default Mapa;
